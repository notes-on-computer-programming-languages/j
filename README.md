# j

Function-level array programming language. http://jsoftware.com

## Official documentation
* https://jsoftware.com
* [*J Wiki*](https://code.jsoftware.com/wiki)
* [*System/Installation/Linux*](https://code.jsoftware.com/wiki/System/Installation/Linux)

## Unofficial documentation
* [*J (programming language)*](https://en.m.wikipedia.org/wiki/J_(programming_language))
* [*Category:J*](https://rosettacode.org/wiki/Category:J)
* [j](https://repology.org/project/j/versions)

## Try it online
* https://tio.run/#j